//stores the data
export const storageSave = (key, value) => {
    if(!key){
        throw new Error('storageSave: No storage key provided')
    }
    if(!value){
        throw new Error('storageSave: No storage valid provided ' + key)
    }

    localStorage.setItem(key, JSON.stringify(value))
}

//reads the data
export const storageRead = key => {

    const data = localStorage.getItem(key)
    if(data){
        return JSON.parse(data)
    }
    return null
}

//Deletes the data
export const storageDelete = key => {
    localStorage.removeItem(key)
}
