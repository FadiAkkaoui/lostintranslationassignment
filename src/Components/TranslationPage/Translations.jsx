import {useForm} from "react-hook-form"
import ProfileNav from "../Hoc/ProfileNav"

const Translation = ({onTranslation}) => {

    const {register, handleSubmit } = useForm()

    const onSubmit = ({ translationNotes }) => { onTranslation( translationNotes )}

    return(
        <>
        <ProfileNav/>
        <br/>
        <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset id="label__translation">
                <label htmlFor="translation-notes" id="label">Translation</label><br/>
                <h5>Please type in your translation below</h5>
                <input type="text" id="input" {...register('translationNotes')} placeholder="Hello"/>
            </fieldset>
            
            <button type="submit" id="btn__translation">Translate</button>

        </form>
        </>
        
    )
}
export default Translation