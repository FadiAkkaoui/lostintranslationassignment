import { useState } from "react"
import '../../App.css';
import 'animate.css';
import { createTranslation} from "../../api/translations"
import { useUser } from "../../Context/UserContext";
import Translation from "./Translations";

const TranslationForm = () => {

    const [images, setImages] = useState([])
    const {user, setUser} = useUser()

    //Function that takes input from user
    //Splits the input to render the images with help of a for-loop
const handleTranslationClicked = async (notes) => {
    

    const [error, result] = await createTranslation(user, notes)

    console.log('Error', error)
    console.log('Result', result)
    //Check if we have a transaltion
    //Combine the coffe with the notes
    //Send an HTTP request
    let input = notes.split('');
    console.log(input)
    let img =[];

    for(let i = 0; i<input.length; i++){
        img.push(<img src = {'individial_signs/' + input[i] + ".png"} alt=""/>) 
    }
    setImages(img)
}
return(
    <>
    <section id="translation-notes">
        <Translation onTranslation={handleTranslationClicked}/>
    </section>
    <fieldset id="img__fieldset">
        {images}
    </fieldset>
    </>
)}

export default TranslationForm