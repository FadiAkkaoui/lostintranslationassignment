const TranslationOutput = ({onTranslationOutput}) => {

    console.log(onTranslationOutput)

    return(
            <fieldset>
                <label htmlFor="translation-output">Translations notes</label>
                <p>({ onTranslationOutput })</p>
            </fieldset>
        
    )
}
export default TranslationOutput