import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import {loginUser} from '../../api/user'
import '../../App.css';
import {  storageSave } from "../../utils/storage";
import {useNavigate} from 'react-router-dom'
import { useUser } from "../../Context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import LoginImg from "../../img/physics.png"
import 'animate.css';
 
const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    // Hooks
    const {
        register, //Used to register inputs to our forms
        handleSubmit, //build in function that is linked to the form submit
        formState: { errors } //Error handler that catches errors
    } = useForm()
    const {user, setUser} = useUser()
    const navigate = useNavigate()

    // Local State
    const [loading, setLoading] = useState(false)
    const  [apiError, setApiError] = useState(null)

    // Side effects
    useEffect(() =>{
        if(user !== null){
            navigate('translationpage')
        }
    }, [user, navigate ]) //Empty deps - Only run 1ce
    
    // Event handlers
    const onSubmit = async ({ username }) => { 
        setLoading(true);
        // Will log null if empty or the us
        const [ error, userResponse ] = await loginUser(username)
        if(error !== null){
            setApiError(error)
        }
        if(userResponse !== null){
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false);
    };


    //Render Functions
    const errorMessage = (() => {
        if(!errors.username) {
            return null
        }

        if(errors.username.type === 'required') {
            return <span>Username is required</span>
        }

        if(errors.username.type === 'minLength'){
            return <span>Username is to short (min. 3)</span>
        }
    })()

    return(
        <>
        <div className="colum__login">
            <div className="box1">
                <img id="img__login" className="animate__animated animate__wobble" src={LoginImg} alt="" />
            </div>
            <div className="box2">
                <h1 className="animate__animated animate__zoomInLeft">
                    Lost in Translation
                </h1>
            </div>
        </div>
        <div id="login__container" className="animate__animated animate__zoomInLeft">
          <h4 className="animate__animated animate__zoomInLeft">Get started</h4>
            <form onSubmit={handleSubmit(onSubmit)}>
                 <label htmlFor="username"></label>
                 <input className="animate__animated animate__zoomInLeft" type="text" placeholder="What's your name?" 
                {...register("username", usernameConfig) } />
       {/*          {(errors.username && errors.username.type === 'required') && <span>Username is required</span>}
                {(errors.username && errors.username.type === 'minLength') && <span>Username is to short (min. 3)</span>} */}
                 <input type="submit" value="Submit" disabled={loading}></input>
                 <br>
                 </br>            
                {errorMessage}
                {loading && <p>Congratulations! Welcome to the club, logging you in...</p>}
                {apiError &&<p> {apiError}</p>}
            </form>
        
        </div>


        </>
    )
}

export default LoginForm