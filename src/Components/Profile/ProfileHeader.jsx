const ProfileHeader = ({username}) => {
    return(
        <>
        <br/>
        <fieldset id="profile__fieldset">
            <h2>Profile</h2>
            <h4>Hello, welcome back {username}</h4>
        </fieldset>
        </>

    )
}
export default ProfileHeader