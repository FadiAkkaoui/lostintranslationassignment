import ProfileHistoryDataItem from "./ProfileHistoryDataItem"

const ProfileTranslationHistory = ({ translations }) => {

    const tenFirstTranslations = translations.slice(-10)

    //Function that maps each element and stores it in the history list 
    const historyList = tenFirstTranslations.map(
        (translation, index) => <ProfileHistoryDataItem key={index + '-' + translation} translation={translation} />)

    return (

        <>
            <h4>Translation history</h4>
            <div className="translationHistory">
                {historyList.length === 0 && <p>You dont have any history </p>}
                <p>{historyList}</p>
            </div>
        </>

    )
}
export default ProfileTranslationHistory