import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../Context/UserContext"
import { storageSave } from "../../utils/storage"
import { translationClearHistory } from "../../api/translations"



const ProfileActions = () => {

    const { user, setUser } = useUser()
    //Function that deletes the history of the user
    const translationClearHistoryClick = async () => {
        if (!window.confirm('Are you sure?\nThis can not be undone!')) {
            return
        }

        const [clearError] = await translationClearHistory(user.id)

        if (clearError !== null) {
            return
        }

        const updateUser = {
            ...user,
            translation: []
        }
        storageSave(STORAGE_KEY_USER ,updateUser)
        setUser(updateUser)

    }

    return (
        <>
            <div className="buttonHolder">
                <button type="button" className="clearHistory" onClick={translationClearHistoryClick}>Clear History</button>
            </div>
        </>

    )
}
export default ProfileActions