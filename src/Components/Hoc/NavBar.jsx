function NavBar() {
  return (
    <>
      <header className="header__header">
        <div id="header__text" className="animate__animated animate__slideInDown">
          Lost in Translation
        </div>
      </header>
    </>
  );
}

export default NavBar;