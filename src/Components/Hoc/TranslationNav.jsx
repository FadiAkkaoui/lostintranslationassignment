import LoginImg from "../../img/physics.png"
import React from 'react';
import {useUser} from "../../Context/UserContext"
import { NavLink } from "react-router-dom"
import {useState} from 'react'

// function that takes data and sets them to the variables 
function TranslationNav() {
  const {user, setUser} = useUser()
  const [input, setData] = useState(null)
  const [img, setImg] = useState();

function onImageChange(data){
}

//Function that takes userinput and splits it, to take each index for the image selection output
const handleSubmit=(e)=>{
  e.preventDefault();
  console.log(input)
  console.log(e)
  let data =input.split('');
  console.log(data)
  onImageChange(data)
}


  return (
<>
<section>
<header className="header__translation">
    <div id="header__text" className="animate__animated animate__slideInDown"><div className="box1">
            </div>Lost in Translation
    <NavLink id="nav__profile" to ='/profile'> Profile</NavLink>
    <img id="img__translation" className="animate__animated animate__wobble" src={LoginImg} alt=""/>
    </div>
    <div className="colum__login">
            <div className="box1">
            </div>
            <div className="box2">
            </div>
        </div>
    </header>
    <div id="translation__container" className="animate__animated animate__zoomInLeft">
            <h4 className="animate__animated animate__zoomInLeft">Welcome {user.username} and {input}</h4>
        <form onSubmit={handleSubmit}>
            <label htmlFor="username"></label>
            <input className="animate__animated animate__zoomInLeft"
                type="text" 
                placeholder="Hello" onChange={(e) =>setData(e.target.value)} />
       {/*          {(errors.username && errors.username.type === 'required') && <span>Username is required</span>}
                {(errors.username && errors.username.type === 'minLength') && <span>Username is to short (min. 3)</span>} */}
            <input type="submit" value="Search"></input>
            <br></br>
        </form>
        <img src={img} alt="" />
        </div>

</section>

    </>
  );
}

export default TranslationNav;