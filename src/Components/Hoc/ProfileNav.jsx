import { useUser } from "../../Context/UserContext"
import { NavLink } from "react-router-dom"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { storageDelete } from "../../utils/storage"

function ProfileNav() {

  const {  setUser } = useUser()

  //When user clicks the button "Log out", this function will be called and delete the storage key attached to the logged in user, and set it to null.
  const handlerLogoutClick = () => {
    if (window.confirm('Are you sure?')) {
         storageDelete(STORAGE_KEY_USER)
         setUser(null)
     }
  }

  return (
    <>
      <ul id='profile__ul' className="animate__animated animate__slideInDown">
         <li id='profile__li'><NavLink to ='/translationpage'>Lost in Translation</NavLink></li>
         <li id='profile__li'><NavLink to ='/translationpage'>Translation</NavLink></li>
         <li id='profile__li'><NavLink to ='/profile'> Profile</NavLink></li>
         <li id='profile__li'  onClick={handlerLogoutClick}><NavLink to ='/profile'>Log Out</NavLink></li>
      </ul>
    </>
  );
}
export default ProfileNav;