import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL

//This functions calls the api and sends the input data from user by user id
// try/catch is used, to handle any errors
export const createTranslation = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                username: user.username,
                translations: [...user.translations, translation]
            })
        })
        if (!response.ok) {
            throw new Error('Could not create your translation ' + translation)
        }
        const result = await response.json()
        return [null, result]
    }
    catch (error) {
        return [error.message, null]
    }

}


export const translationClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })

        })
        if (!response.ok) {
            throw new Error('Could not update translation')
        }
        const result = await response.json()
        return [null, result]
    }
    catch (error) {
        return [error.message, null]
    }
}


