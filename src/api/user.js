import { createHeaders } from "./index"

const apiUrl = process.env.REACT_APP_API_URL

// Function that checks if user exists in API, try/catch is used to identify any errors
export const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if (!response.ok) {
            throw new Error('Could not complete request.')
        }
        const data = await response.json()
        return [null, data]
    }
    catch (error) {
        return [error.message, []]
    }
}

// Function that creates a user
export const createUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            //POST to API and create a new user method
            method: 'POST', //Create a resourse
            headers: createHeaders(),
            //.stringify converts to string
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error('Could not create user with username ' + username)
        }
        const data = await response.json()
        return [null, data]
    }
    catch (error) {
        return [error.message, []]
    }
}

//Function that checks if the user how does a log in is registered or not in the database, depending on result it will either send it to createUser or log user in.
export const loginUser = async (username) => {
    const [checkError, user] = await checkForUser(username)

    // If something went wrong with the user or not found user
    if (checkError !== null) {
        return [checkError, null]
    }
    // If nothing went wrong and we have user in database
    if (user.length > 0) {
        // user exists.
        //Returns either null or user
        return [null, user.pop()]
    }

    return await createUser(username)

}
export const userById = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`)
        if (!response.ok) {
            throw new Error('Could not fetch user')
        }

        const user = await response.json()
        return [null, user]

    }
    catch (error) {
        return [error.message, null]

    }
}

