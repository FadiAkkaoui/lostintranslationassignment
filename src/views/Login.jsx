import LoginForm from "../Components/Login/LoginForm"
import '../App.css';
import NavBar from "../Components/Hoc/NavBar";


const Login = () => {
    return (
        <>
            <NavBar/>
            <LoginForm/>
        </>
    )
}

export default Login