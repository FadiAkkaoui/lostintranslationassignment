import withAuth from "../Components/Hoc/withAuth"
import ProfileHeader from "../Components/Profile/ProfileHeader"
import ProfileActions from "../Components/Profile/ProfileActions"
import ProfileTranslationHistory from "../Components/Profile/ProfileTranslationHistory"
import { useUser } from "../Context/UserContext"
import ProfileNav from "../Components/Hoc/ProfileNav"
import { useEffect } from "react"
import { userById } from "../api/user"
import { storageRead, storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"

const Profile = () => {


    const { user, setUser} = useUser()

    useEffect(() => {

        const findUser = async () => {
            const[error, latesUser] = await userById(user.id)
            if(error === null){
                storageSave(STORAGE_KEY_USER, latesUser)
                setUser(latesUser)
            }
        }
        findUser()
    },[setUser, user.id])
    

    return (
        <>
            <ProfileNav/>
            <ProfileHeader username={user.username} />
            <ProfileTranslationHistory translations={user.translations} /> 
            <ProfileActions/>

        </>
    )
}
export default withAuth(Profile)