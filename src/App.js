import './App.css';
import{
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import Login from './views/Login';
import Profile from './views/Profile';
import TranslationPage from './views/TranslationPage';


function App() {
  return (
    <BrowserRouter> 
        <div className="App">
        <Routes>
          <Route path="/" element={<Login/>}/>
          <Route path="/translationPage" element={<TranslationPage/>}/>
          <Route path="/profile" element={<Profile/>}/>
        </Routes>
        <footer>
        </footer>
        </div>
    </BrowserRouter>
    
  );
}

export default App;
