//Context -> Exposing
import { createContext, useContext, useState} from 'react'
import { STORAGE_KEY_USER } from '../const/storageKeys'
import { storageRead } from '../utils/storage'

// Context -> exposing
const TranslationContext = createContext()

export const useTranslation = ()=> {
    return useContext(TranslationContext) // {user, setUser}
}

//Provider -> managing Stater
//Sets the to the local storage 
const TranslationProvider = ({children}) => {
    const[translations, setTranslation] = useState(storageRead(STORAGE_KEY_USER))
    const state = {
        translations,
        setTranslation
    }
    return (
        <TranslationContext.Provider value={state}>
            {children}
        </TranslationContext.Provider>
    )
}

export default TranslationProvider